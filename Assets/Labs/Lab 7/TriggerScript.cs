using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnColliderEnter2D(Collision2D col)
    {
        Debug.Log(col.gameObject.name + " entered a trigger with me!");
    }

    void OnTriggerStay2D(Collider2D other) 
    { 
        Debug.Log(other.gameObject.name + " is in a trigger with me!"); 
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log(other.gameObject.name + " left a trigger with me!");
    }
}
