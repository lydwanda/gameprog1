using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public Vector3 begin;
    public Vector3 end;
    public Vector3 targetPosition;
    public float movementSpeed;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = begin;

        targetPosition = end;
    }

    // Update is called once per frame
    void Update()
    {

        float move = movementSpeed * Time.deltaTime;

        if ((transform.position - targetPosition).magnitude < .001)
        {
            targetPosition = ((targetPosition - end).magnitude < .001) ? begin : end;
        }

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, move);

    }
}
