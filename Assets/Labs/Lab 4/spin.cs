using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spin : MonoBehaviour
{
    public float angle;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        angle = speed * Time.deltaTime;
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).RotateAround(transform.position, Vector3.forward, angle);
        }
    }
}
