using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimScript : MonoBehaviour
{
    public GameObject cannon;

    // Start is called before the first frame update
    void Start()
    {
        if (cannon == null)
        {
            Debug.LogError("You forogt to set the prefab");
        }
    }

    // Update is called once per frame
    void Update()
    {
        Animator anim = GetComponent<Animator>();
        if (Input.GetButtonDown("Jump"))
        {
            anim.SetBool("Cannon", true);
        }
    }

    public void SpawnCannon(Object nullObject)
    {
        GameObject obj = GameObject.Instantiate<GameObject>(cannon);
        Destroy(obj, 2);
    }

    public void SwitchTransition(Object nullObj)
    {
        Animator anim = GetComponent<Animator>();
        anim.SetBool("Cannon", false);
    }
}
