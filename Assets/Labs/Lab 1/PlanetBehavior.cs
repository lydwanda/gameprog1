using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetBehavior : MonoBehaviour
{
    public float xSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float deltaSpeed = xSpeed * Time.deltaTime; 
        transform.position = new Vector3(transform.position.x + xSpeed, transform.position.y, transform.position.z);

        transform.Rotate(new Vector3(transform.position.x, transform.position.y, transform.position.z + 100) * Time.deltaTime);
    }
}
