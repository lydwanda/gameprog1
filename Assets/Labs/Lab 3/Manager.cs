using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public float movementSpeed;
    public float rotationSpeed;
    public float scalingSpeed;
}
