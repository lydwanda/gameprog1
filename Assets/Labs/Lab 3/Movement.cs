using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    GameObject obj;

    public Vector3 begin;
    public Vector3 end;
    public Vector3 targetPosition;

    public Vector3 minSize = new Vector3(.3f, .3f, .3f);
    public Vector3 maxSize = new Vector3(1.3f, 1.3f, 1.3f);
    public Vector3 targetScale;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = begin;
        transform.localScale = minSize;

        targetPosition = end;
        targetScale = maxSize;
    }

    // Update is called once per frame
    void Update()
    {
        obj = GameObject.FindGameObjectWithTag("GameManager");
        Manager sm = obj.GetComponent<Manager>();

        float move = sm.movementSpeed * Time.deltaTime;
        float scale = sm.scalingSpeed * Time.deltaTime;
        if ((transform.position - targetPosition).magnitude < .001)
        {
            // (condition) ? (value if true) : (value if false)
            targetPosition = ((targetPosition - end).magnitude < .001) ? begin : end;

            //if ((targetPosition - end).magnitude < .001)
            //{
            //    targetPosition = begin;
            //}
            //else
            //{
            //    targetPosition = end;
            //}
        }

        if ((transform.localScale - targetScale).magnitude < .001)
        {
            targetScale = ((targetScale - maxSize).magnitude < .001) ? minSize : maxSize;

            //if ((targetScale - maxSize).magnitude < .001)
            //{
            //    targetScale = minSize;
            //}
            //else 
            //{
            //    targetScale = maxSize;
            //}
        }
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, move);
        transform.Rotate(new Vector3(0.0f, 0.0f, 5 * sm.rotationSpeed) * Time.deltaTime);
        transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, scale);

        //transform.localScale += new Vector3(sm.scalingSpeed * Time.deltaTime, sm.scalingSpeed * Time.deltaTime, 0.0f);
        //transform.position = new Vector3(transform.position.x - sm.movementSpeed, transform.position.y, transform.position.z);
        //transform.Rotate(new Vector3(0.0f, 0.0f, 5 * sm.rotationSpeed) * Time.deltaTime);
        //transform.localScale -= new Vector3(sm.scalingSpeed * Time.deltaTime, sm.scalingSpeed * Time.deltaTime, 0.0f);
    }
}
